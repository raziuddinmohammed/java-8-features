package foreach;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class Java8ForEachExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// creating a sample collection
		List<Integer> myList = new ArrayList<Integer>();
		for (int i = 0; i < 3; i++) {
			myList.add(i);
		}

		// traversing using iterator
		System.out.println("----------------------------");
		System.out.println("traversing using iterator");
		System.out.println("----------------------------");
		Iterator<Integer> it = myList.iterator();
		while (it.hasNext()) {
			Integer i = it.next();
			
			System.out.println("Iterator value:: " + i);
		}

		// traversing through forEach() method of Iterable with anonymous class
		System.out.println("----------------------------");
		System.out.println("traversing through forEach() method of Iterable with anonymous class");
		System.out.println("----------------------------");
		myList.forEach(new Consumer<Integer>() {

			@Override
			public void accept(Integer t) {
				// TODO Auto-generated method stub
				System.out.println("For each anonymous class Value:: " + t);

			}

		});
		
		// traversing with Consumer interface implementation
		System.out.println("----------------------------");
		System.out.println("traversing with Consumer interface implementation");
		System.out.println("----------------------------");
		MyConsumer action = new MyConsumer();
		myList.forEach(action);

	}

}

// consumer implementation that can be reused
class MyConsumer implements Consumer<Integer>{

	@Override
	public void accept(Integer t) {
		// TODO Auto-generated method stub
		System.out.println("Consumer implementation Value:: " + t);
	}
	
}
